"use strict";

const path = require("path");
const CompressionWebpackPlugin = require('compression-webpack-plugin');
const OptimizeCssnanoPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

const defaultSettings = require("./src/settings.js");

function resolve(dir) {
  return path.join(__dirname, dir);
}
const pro = process.env.NODE_ENV === 'production';
const name = defaultSettings.title; // page title
// If your port is set to 80,
// use administrator privileges to execute the command line.
// For example, Mac: sudo npm run
// You can change the port by the following methods:
// port = 9528 npm run dev OR npm run dev --port = 9528
const port = process.env.port || process.env.npm_config_port || 9528; // dev port
module.exports = {
  publicPath: '/',
  outputDir: "dist",
  assetsDir: "static",
  runtimeCompiler: true,
  productionSourceMap: false,
  lintOnSave: false,
  devServer: {
    port: port,
    open: true,
    proxy: process.env.VUE_APP_apiUrl,
    https: false,
    overlay: {
      warnings: false,
      errors: true,
    },
    // before: require('./mock/mock-server.js'),
  },
  configureWebpack: config => {
    if(!pro) return
    return {
      name: name,
      resolve: {
        alias: {
          "@": resolve("src")
        }
      },
      plugins: [
        // 使用 UglifyJsPlugin 并行压缩输出JS代码 https://www.webpackjs.com/plugins/uglifyjs-webpack-plugin/
        new UglifyJsPlugin({
          cache: true,
          uglifyOptions: {
            output: {
              comments: false,
              beautify: false,
            },
            warnings: false
          }
        }),
        // 压缩和去重css样式文件
        new OptimizeCssnanoPlugin({
          sourceMap: false,
          cssnanoOptions: {
            preset: [
              'default',
              {
                mergeLonghand: false,  //合并属性
                cssDeclarationSorter: false  //根据css的属性名称进行排序
              }
            ]
          },
        }),
        // gzip
        new CompressionWebpackPlugin({
          filename: '[path].gz[query]',
          algorithm: 'gzip',
          test: /\.(js|css|html|svg)$/i,
          threshold: 2048,
          minRatio: 0.8
        }),
      ]
    }
  },
  chainWebpack(config) {
    config.plugins.delete("prefetch"); // TODO: need test
    // 可以提高第一个屏幕的速度，建议打开预加载
    config.plugin('preload').tap(() => [
      {
        rel: 'preload',
        // to ignore runtime.js
        // https://github.com/vuejs/vue-cli/blob/dev/packages/@vue/cli-service/lib/config/app.js#L171
        fileBlacklist: [/\.map$/, /hot-update\.js$/, /runtime\..*\.js$/],
        include: 'initial'
      }
    ])

    // set svg-sprite-loader
    config.module
      .rule("svg")
      .exclude.add(resolve("src/icons"))
      .end();
    config.module
      .rule("icons")
      .test(/\.svg$/)
      .include.add(resolve("src/icons"))
      .end()
      .use("svg-sprite-loader")
      .loader("svg-sprite-loader")
      .options({
        symbolId: "icon-[name]"
      })
      .end();

    // set preserveWhitespace
    config.module
      .rule("vue")
      .use("vue-loader")
      .loader("vue-loader")
      .tap(options => {
        options.compilerOptions.preserveWhitespace = true;
        return options;
      })
      .end();

    config
      // https://webpack.js.org/configuration/devtool/#development
      .when(process.env.NODE_ENV === "development", config => {
        config.devtool("cheap-source-map")
      });

    config.when(process.env.NODE_ENV !== "development", config => {
      config
        .plugin("ScriptExtHtmlWebpackPlugin")
        .after("html")
        .use("script-ext-html-webpack-plugin", [
          {
            // `runtime` must same as runtimeChunk name. default is `runtime`
            inline: /runtime\..*\.js$/
          }
        ])
        .end();
      config.optimization.splitChunks({
        chunks: "all",
        cacheGroups: {
          libs: {
            name: "chunk-libs",
            test: /[\\/]node_modules[\\/]/,
            priority: 10,
            chunks: "initial" // only package third parties that are initially dependent
          },
          elementUI: {
            name: "chunk-elementUI", // split elementUI into a single package
            priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
            test: /[\\/]node_modules[\\/]_?element-ui(.*)/ // in order to adapt to cnpm
          },
          commons: {
            name: "chunk-commons",
            test: resolve("src/components"), // can customize your rules
            minChunks: 3, //  minimum common number
            priority: 5,
            reuseExistingChunk: true
          }
        }
      });
      config.optimization.runtimeChunk("single");
    });
  }
};
