//获取动态路由方法
import Layout from '@/layout';
// 权限路由表
export const AsyncRouterMap = [
	{
		path: '/auth',
		component: Layout,
		meta: { title: '权限管理', icon: 'form' },
		children: [
			{
				path: '',
				name: 'auth',
				component: () => import('@/views/auth/index'),
				meta: { title: '权限管理111', icon: 'form', code: 'A1' }
			},
			{
				path: '/auth2',
				name: 'auth2',
				component: () => import('@/views/auth/index'),
				meta: { title: '权限管理2', icon: 'form', code: 'A2' }
			},
		]
	},
];
function getRouterList() {
	//登录之后获取动态路由
	let role_router;
	let userRouters = [];
	role_router = sessionStorage.getItem('role_router');
	// 超级管理员
	if(role_router === '*') {
		return userRouters.concat(AsyncRouterMap)
	}
	let userRouter = JSON.parse(role_router);
	AsyncRouterMap.map((value, i) => {
		let child = [];
		if (
			(value.meta && value.meta.code && userRouter && userRouter.includes(value.meta.code)) ||
			(value.meta && userRouter && !value.meta.code)
		) {
			value.children.forEach((item, i) => {
				if (
					(item.meta && item.meta.code && userRouter.includes(item.meta.code)) ||
					(item.meta && !item.meta.code)
				) {
					child.push(item);
				}
			});
			if (child.length > 0) {
				value.children = child;
			}
			userRouters.push(value);
		}
	});
	return userRouters;
}

export default getRouterList;
