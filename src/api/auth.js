import api from "@/plugins/axios";

// 角色列表--选择下拉框也用这个
export function getRoleList(data) {
    return api({
        url: '/role/listRole',
        method: 'post',
        data,
    });
}
// 新增或编辑
export function addOrUpdateRole(data) {
    return api({
        url: '/role/addOrUpdateRole',
        method: 'post',
        data,
    });
}
// 修改角色数据权限
export function rolePermissions(data) {
    return api({
        url: '/role/rolePermissions',
        method: 'post',
        data,
    });
}
// 修改角色数据权限;
export function updateRoleDataAuth(data) {
    return api({
        url: '/role/updateRoleDataAuth',
        method: 'post',
        data,
    });
}
// 修改角色菜单权限;
export function updateRolePermissions(data) {
    return api({
        url: '/role/updateRolePermissions',
        method: 'post',
        data,
    });
}
