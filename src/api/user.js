import api from "@/plugins/axios";

export function login(data) {
  return api({
    url: "/userLogin",
    method: 'post',
    data,
  });
}
export function logout(data) {
  return api({
    url: "/userLogout",
    method: 'post',
    data,
  });
}
export function getInfo(data) {
  return api({
    url: "/getUserInfo",
    method: 'post',
    data,
  });
}
